import io

from setuptools import find_packages
from setuptools import setup

with io.open('README.md', 'rt', encoding='utf8') as f:
    readme = f.read()

setup(
    name='hongissocute',
    version='0.0.1',
    url='',
    license='BSD',
    maintainer='Haruethai Zea-Han',
    maintainer_email='6110110497.psu.ac.th',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[''],
    extras_require={'test': ['nose', 'coverage']},

)

