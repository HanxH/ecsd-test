class FizzBuzzManager():
    def callfizzbuzz(self):
        fizzbuzzcondition = [
            FizzFizzBuzzBuzz(),
            FizzBuzz(),
            FizzFizz(),
            BuzzBuzz(),
            Fizz(),
            Buzz(),
            NoFizzBuzz(),
        ]
        return SuperFizzBuzz(fizzbuzzcondition)


class SuperFizzBuzz():
    def __init__(self, fizzbuzzcondition):
        self.fizzbuzzcondition = fizzbuzzcondition

    def result(self, n):
        for Condition in self.fizzbuzzcondition:
            if Condition.is_manage(n):
                return Condition.result(n)


class Condition():
    def is_manage(self, n):
        pass

    def result(self, n):
        pass


class NoFizzBuzz(Condition):
    def is_manage(self, n):
        return True

    def result(self, n):
        return "NoFizzBuzz"


class FizzBuzz(Condition):
    def is_manage(self, n):
        return n % 5 == 0 and n % 3 == 0

    def result(self, n):
        return "FizzBuzz"


class Fizz(Condition):
    def is_manage(self, n):
        return n % 3 == 0

    def result(self, n):
        return "Fizz"


class Buzz(Condition):
    def is_manage(self, n):
        return n % 5 == 0

    def result(self, n):
        return "Buzz"


class FizzFizz(Condition):
    def is_manage(self, n):
        return n % 9 == 0

    def result(self, n):
        return "FizzFizz"


class BuzzBuzz(Condition):
    def is_manage(self, n):
        return n % 25 == 0

    def result(self, n):
        return "BuzzBuzz"


class FizzFizzBuzzBuzz(Condition):
    def is_manage(self, n):
        return n % 9 == 0 and n % 25 == 0

    def result(self, n):
        return "FizzFizzBuzzBuzz"


# try:
#     n = int(input('Number : '))
#     if n in range(10001):
#         result = FizzBuzzManager().callfizzbuzz()
#         print(result.result(n))
#     else:
#         print("NoFizzBuzz")
# except:
#     print("NoFizzBuzz")
